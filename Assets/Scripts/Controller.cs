using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public GameObject Field;
    public GameObject CheckerWhite;
    public GameObject CheckerBlack;
    private int[,] checkersPosition =
    {
        {0, 2, 0, 2, 0, 2, 0, 2},
        {2, 0, 2, 0, 2, 0, 2, 0},
        {0, 2, 0, 2, 0, 2, 0, 2},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 1, 0, 1, 0, 1, 0},
        {0, 1, 0, 1, 0, 1, 0, 1},
        {1, 0, 1, 0, 1, 0, 1, 0}
    };
    void Start()
    {


        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                var field = Instantiate(Field).GetComponent<CheckerField>();
                field.ChangeColor((i + j) % 2 == 0);
                field.transform.position = new Vector3(i - 4, 0, j -4);

                Debug.Log("i" + i +" "+ "j" + j + " " + checkersPosition [i, j]);
                
                if (checkersPosition[i, j]==2)
                {
                    var checker = Instantiate(CheckerWhite);
                    checker.transform.position = new Vector3(i - 4, 0.64f, j - 4);

                }
                if (checkersPosition[i, j] == 1)
                {
                    var checker = Instantiate(CheckerBlack);
                    checker.transform.position = new Vector3(i - 4, 0.64f, j - 4);
                }
            }

                // if ((i + j) % 2 == 0)
                // {
                //     spriteRenderer.color = Color.white;
                // }
                // else
                // {
                //     spriteRenderer.color = Color.black;
                // }
        }


            /*
            var testField = Instantiate(Field).GetComponent<CheckerField>();
            testField.ChangeColor(false);
            */

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
